let mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.combine([
        'resources/assets/vendor/js/jquery.min.js',
        'resources/assets/vendor/js/popper.min.js',
        'resources/assets/vendor/js/bootstrap.min.js',
        'node_modules/jquery-validation/dist/jquery.validate.min.js',
        'node_modules/jquery-validation/dist/additional-methods.js'
    ], 'public/js/bundle.js')
    .js('resources/assets/js/app.js', 'public/js')
    .js('resources/assets/js/main.js', 'public/js')
    .combine([
        'resources/assets/vendor/css/normalize.css',
        'resources/assets/vendor/css/font-awesome.min.css',
        'resources/assets/vendor/css/bootstrap.min.css'
    ], 'public/css/vendor.css')
    .sass('resources/assets/sass/app.scss', 'public/css');
