@extends('layouts.app')

@section('content')
<section id="upload-section">
    
    <form id="upload-form" enctype="multipart/form-data" method="POST" action="{{ url('store') }}" class="needs-validation" novalidate>
        {{ csrf_field() }}
        <div class="form-group">
            <label for="title">Title *</label>
            <input type="text" class="form-control" id="title" name="title">
            <div class="invalid-feedback"></div>
        </div>
        <div class="form-group">
            <label for="author">Author *</label>
            <input type="text" class="form-control" id="author" name="author">
            <div class="invalid-feedback"></div>
        </div>
        <div class="form-group ">
            <label for="file">File *</label>
            <div class="custom-file">
                <input type="file" class="custom-file-input" id="file" name="file">
                <label class="custom-file-label" for="file">Choose file</label>
                <div class="invalid-feedback"></div>
            </div>
        </div>
        <div class="row">
            <div class="col-auto">
                <button class="btn btn-primary" type="submit">Upload</button>
            </div>
            <div class="col align-self-center">
                <div class="progress">
                    <div class="progress-bar" role="progressbar" style="width: 0%" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100"></div>
                </div>
            </div>
        </div>
    </form>
</section>

@endsection
