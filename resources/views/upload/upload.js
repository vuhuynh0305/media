export default {
        run() {
            var self = this;
            this.onChangeInputFile();
            this.validateUploadForm();
            this.submitForm();
        },
        
        onChangeInputFile() {
            $("#file").on("change", function() {
                var filename = $(this).val().split("\\").pop();
                $(this).next('label').text(filename);
            })
        },
        
        validateUploadForm() {
            $("#upload-form").validate({
                rules: {
                    title: {
                        required: true
                    },
                    author: {
                        required: true
                    },
                    file: {
                        required: true,
                        accept: "audio/*, video/*"
                    }
                },
                messages: {
                    file: {
                        accept: 'This field requires media file'
                    }
                },
                errorClass: "is-invalid",
                errorPlacement: function(error, element) {
                    $(element).addClass("is-invalid");
                    $(element).parent().find('div.invalid-feedback').text(error.text());
                },
                success: function(label,element) {
                    $(element).addClass("is-valid");
                }
            });
        },
        
        submitForm() {
            $('#upload-form').submit(function(e) {
                if ($(this).valid()){
                    e.preventDefault();
                    $(this).find('button').attr('disabled', true);
                    var fd = new FormData(this);

                    $.ajax({
                        url: $(this).attr('action'),
                        xhr: function() {
            
                            var xhr = new XMLHttpRequest();
                            var total = 0;
            
                            $.each(document.getElementById('file').files, function(i, file) {
                                total += file.size;
                            });
            
                            xhr.upload.addEventListener("progress", function(evt) {
                                var loaded = Math.floor((evt.loaded / total)*100); // percent
                                $('.progress-bar').css('width', loaded + '%').attr('aria-valuenow', loaded).text(loaded + '%');
                            }, false);
            
                            return xhr;
                        },
                        type: $(this).attr('method'),
                        processData: false,
                        contentType: false,
                        data: fd,
                        success: function() {
                            location.href="/music"
                        }
                    });
                }
            });
        }
    };
