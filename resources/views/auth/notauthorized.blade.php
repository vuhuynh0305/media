@extends('layouts.app')

@section('content')
<h1 class="text-center text-warning">You are not authorized to view this page</h1>
@endsection
