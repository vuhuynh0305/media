@extends('layouts.app')

@section('content')

<div class="row">
    <div class="col s6">
        <form method="POST" action="{{ route('register') }}" novalidate>
            {{ csrf_field() }}
            <div class="row">
                <div class="input-field col s12">
                    <input id="name" type="text" name="name" value="{{ old('name') }}" class="validate {{ $errors->has('name') ? 'invalid':'' }}" />
                    <label for="name">Name</label>
                    <span class="invalid-feedback">{{ $errors->has('name') ? $errors->first('name'):'' }}</span>
                </div>
                <div class="input-field col s12">
                    <input id="username" type="text" name="username" value="{{ old('username') }}" class="validate {{ $errors->has('username') ? 'invalid':'' }}" />
                    <label for="username">Username</label>
                    <span class="invalid-feedback">{{ $errors->has('username') ? $errors->first('username'):'' }}</span>
                </div>
                <div class="input-field col s12">
                    <input id="email" type="email" name="email" value="{{ old('email') }}" class="validate {{ $errors->has('email') ? 'invalid':'' }}" />
                    <label for="email">Email</label>
                    <span class="invalid-feedback">{{ $errors->has('email') ? $errors->first('email'):'' }}</span>
                </div>
                <div class="input-field col s12">
                    <input id="password" type="password" name="password" value="{{ old('password') }}" class="validate {{ $errors->has('password') ? 'invalid':'' }}">
                    <label for="password">Password</label>
                    <span class="invalid-feedback">{{ $errors->has('password') ? $errors->first('password'):'' }}</span>
                </div>
                <div class="input-field col s12">
                    <input id="password_confirmation" type="password" name="password_confirmation" class="validate" />
                    <label for="password_confirmation">Confirm password</label>
                </div>
                <div class="input-field col s12">
                    <button type="submit" class="waves-effect waves-light btn">Register</button>
                </div>
            </div>
        </form>
    </div>
</div>
@endsection
