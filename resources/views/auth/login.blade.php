@extends('layouts.app')

@section('content')
<div class="row">
    <div class="col s6">
        <form method="POST" action="{{ route('login') }}" novalidate>
            {{ csrf_field() }}
            <div class="row">
                <div class="input-field col s12">
                    <input id="username" type="text" name="username" value="{{ old('username') }}" class="validate {{ $errors->has('username') ? 'invalid':'' }}" />
                    <label for="username">Username</label>
                    <span class="invalid-feedback">{{ $errors->has('username') ? $errors->first('username'):'' }}</span>
                </div>
                <div class="input-field col s12">
                    <input id="password" type="password" name="password" value="{{ old('password') }}" class="validate {{ $errors->has('password') ? 'invalid':'' }}">
                    <label for="password">Password</label>
                    <span class="invalid-feedback">{{ $errors->has('password') ? $errors->first('password'):'' }}</span>
                </div>
                <p class="col s12">
                    <input type="checkbox" class="filled-in" id="remember" name="remember" checked="{{ old('remember') ? 'checked' : '' }}" />
                    <label for="remember">Remember me</label>
                </p>
                <div class="input-field col s12">
                    <button type="submit" class="waves-effect waves-light btn">Login</button>
                    <a href="{{ route('password.request') }}">
                        Forgot Your Password?
                    </a>
                </div>
            </div>
        </form>
    </div>
</div>




<!--<div>
    <div class="cell-6">
        <form method="POST" action="{{ route('login') }}" novalidate>
            {{ csrf_field() }}
            <div class="form-group">
                <label for="username">Username</label>
                <input id="username" type="text" name="username" value="{{ old('username') }}" autofocus class="{{ $errors->has('username') ? 'invalid' : ''}}" />
                @if ($errors->has('username'))
                <span class="invalid_feedback">
                    {{$errors->first('username')}}
                </span>
                @endif
            </div>
            <div class="form-group">
                <label for="password">Password</label>
                <input id="password" type="password" name="password" class="{{ $errors->has('password') ? 'invalid' : ''}}">
                @if ($errors->has('password'))
                <span class="invalid_feedback">
                    {{$errors->first('password')}}
                </span>
                @endif
            </div>
            <div class="form-group">
                <input type="checkbox" data-role="checkbox" data-caption="Remember me" name="remember" {{ old('remember') ? 'checked' : '' }}>
            </div>
            <div class="form-group">
                <button type="submit" class="button success">
                    Login
                </button>
                
                <a class="ml-1" href="{{ route('password.request') }}">
                    Forgot Your Password?
                </a>
            </div>
        </form>
    </div>
</div>-->
@endsection

@section("scripts")
<script>
    $(document).ready(function() {
        Materialize.updateTextFields();
    });
</script>
@endsection
