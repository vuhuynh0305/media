@extends('layouts.app')

@section('content')

<section id="music-section">
    <canvas id="canvas" style="width: 100%"></canvas>
    <!--<audio src="{{ asset('files/bongbong.mp3') }}" controls id="player"></audio>-->
    <div class="music-control valign-wrapper">
        <div class="music-play d-inline-block">
            <button type="button" id="playBtn" class="material-icons">play_arrow</button>
        </div>
        <div class="music-seekbar d-inline-block">
            <input type="range" step="any" id="seekbar" value="0">
        </div>
        <div class="music-duration d-inline-block">
            <span id="currentTime">00:00</span>/<span id="duration"></span>
        </div>
        <div class="music-loop d-inline-block">
            <button type="button" id="loopBtn" class="material-icons">loop</button>
        </div>
        <div class="music-volume d-inline-block">
            <button id="muteBtn" class="material-icons d-inline-block">volume_down</button>
            <input type="range" class="d-inline-block" step="any" id="volume" value="0.5" min="0" max="1">
        </div>
    </div>
</section>

@endsection
