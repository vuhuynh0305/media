export default {
        
        run() {
//            var self = this;
//            this.togglePlay();
//            this.loadAudio();
//            this.loadVisualize();
//            this.loopAudio();
//            this.setVolume(self);
//            this.muteVolume(self);
        },
        
        togglePlay() {
            $("#playBtn").on("click", function() {
                var audio = $("#player").get(0);
                
                if(audio.paused) {
                    audio.play();
                    $(this).text("pause");
                }
                else {
                    audio.pause();
                    $(this).text("play_arrow");
                }
            });            
        },
        
        loadAudio() {
            var audio = $("#player").get(0);
            var seekbar = $("#seekbar");
            var currentTime = $("#currentTime");
            var duration = $("#duration");
            
            function formatTime (seconds) {
                var minutes = Math.floor(seconds / 60);
                minutes = (minutes >= 10) ? minutes : "0" + minutes;
                seconds = Math.floor(seconds % 60);
                seconds = (seconds >= 10) ? seconds : "0" + seconds;
                return minutes + ":" + seconds;
            }
            
            audio.onloadedmetadata = function() {
                seekbar.attr('max', audio.duration);
                duration.text(formatTime(audio.duration));
            };
            seekbar.on('change', function() {
                audio.currentTime = seekbar.val();
            });
            
            audio.ontimeupdate = function() {
                seekbar.val(audio.currentTime);
                currentTime.text(formatTime(audio.currentTime));
            };
            
            audio.volume = 0.5;
        },
        
        loopAudio() {
            $("#loopBtn").on("click",function() {
                var audio = $("#player").get(0);
                if (audio.loop === false) {
                    audio.loop = true;
                    $(this).addClass("active");
                }
                else {
                    audio.loop = false;
                    $(this).removeClass("active");
                };
            });
        },
        
        muteVolume(self) {
            $("#muteBtn").on("click", function() {
                var audio = $("#player").get(0);
                if (audio.muted === false) {
                    audio.muted = true;
                }
                else {
                    audio.muted = false;
                };
                self.setVolumeIcon();
            });
        },
        
        setVolume(self) {
            $("#volume").on("input", function() {
                var audio = $("#player").get(0);
                var volume = $(this).val();
                audio.volume = volume;
                self.setVolumeIcon();
            });
        },
        
        setVolumeIcon() {
            var audio = $("#player").get(0);
            var volume = audio.volume;
            var muteBtn = $("#muteBtn");
            if (audio.muted) {
                muteBtn.text("volume_off");
            }
            else if (volume > 0.6) {
                muteBtn.text("volume_up");
            }
            else if (volume > 0.1) {
                muteBtn.text("volume_down");
            }
            else {
                muteBtn.text("volume_mute");
            }
        },

        loadVisualize() {
            var audio = $("#player").get(0);
            var context = new AudioContext();
            var src = context.createMediaElementSource(audio);
            var analyser = context.createAnalyser();

            var canvas = document.getElementById("canvas");
            canvas.width = window.innerWidth;
            canvas.height = 300;
            var ctx = canvas.getContext("2d");

            src.connect(analyser);
            analyser.connect(context.destination);

            analyser.fftSize = 256;

            var bufferLength = analyser.frequencyBinCount;

            var dataArray = new Uint8Array(bufferLength);

            var WIDTH = canvas.width;
            var HEIGHT = canvas.height;

            var barWidth = (WIDTH / bufferLength) * 1.5;
            var barHeight;
            var x = 0;

            function renderFrame() {
                requestAnimationFrame(renderFrame);

                x = 0;

                analyser.getByteFrequencyData(dataArray);

                ctx.fillStyle = "#000";
                ctx.fillRect(0, 0, WIDTH, HEIGHT);

                for (var i = 0; i < bufferLength; i++) {
                    barHeight = dataArray[i];

                    var r = barHeight + (25 * (i/bufferLength));
                    var g = 250 * (i/bufferLength);
                    var b = 0;

                    ctx.fillStyle = "darkcyan";
                    ctx.fillRect(x, HEIGHT - barHeight, barWidth, barHeight);
                    x += barWidth + 1;
                }
            }
            renderFrame();
        }


    };
