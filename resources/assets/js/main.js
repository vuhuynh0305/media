import upload from '../../views/upload/upload';
import music from '../../views/music/music';

$(() => {
    var url = window.location.pathname;
    if (url === "/upload") {
      upload.run();
    }
    if (url === "/music") {
      music.run();
    }
});
