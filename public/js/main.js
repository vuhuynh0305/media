/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 42);
/******/ })
/************************************************************************/
/******/ ({

/***/ 42:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(43);


/***/ }),

/***/ 43:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__views_upload_upload__ = __webpack_require__(44);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__views_music_music__ = __webpack_require__(45);



$(function () {
  var url = window.location.pathname;
  if (url === "/upload") {
    __WEBPACK_IMPORTED_MODULE_0__views_upload_upload__["a" /* default */].run();
  }
  if (url === "/music") {
    __WEBPACK_IMPORTED_MODULE_1__views_music_music__["a" /* default */].run();
  }
});

/***/ }),

/***/ 44:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony default export */ __webpack_exports__["a"] = ({
    run: function run() {
        var self = this;
        this.onChangeInputFile();
        this.validateUploadForm();
        this.submitForm();
    },
    onChangeInputFile: function onChangeInputFile() {
        $("#file").on("change", function () {
            var filename = $(this).val().split("\\").pop();
            $(this).next('label').text(filename);
        });
    },
    validateUploadForm: function validateUploadForm() {
        $("#upload-form").validate({
            rules: {
                title: {
                    required: true
                },
                author: {
                    required: true
                },
                file: {
                    required: true,
                    accept: "audio/*, video/*"
                }
            },
            messages: {
                file: {
                    accept: 'This field requires media file'
                }
            },
            errorClass: "is-invalid",
            errorPlacement: function errorPlacement(error, element) {
                $(element).addClass("is-invalid");
                $(element).parent().find('div.invalid-feedback').text(error.text());
            },
            success: function success(label, element) {
                $(element).addClass("is-valid");
            }
        });
    },
    submitForm: function submitForm() {
        $('#upload-form').submit(function (e) {
            if ($(this).valid()) {
                e.preventDefault();
                $(this).find('button').attr('disabled', true);
                var fd = new FormData(this);

                $.ajax({
                    url: $(this).attr('action'),
                    xhr: function xhr() {

                        var xhr = new XMLHttpRequest();
                        var total = 0;

                        $.each(document.getElementById('file').files, function (i, file) {
                            total += file.size;
                        });

                        xhr.upload.addEventListener("progress", function (evt) {
                            var loaded = Math.floor(evt.loaded / total * 100); // percent
                            $('.progress-bar').css('width', loaded + '%').attr('aria-valuenow', loaded).text(loaded + '%');
                        }, false);

                        return xhr;
                    },
                    type: $(this).attr('method'),
                    processData: false,
                    contentType: false,
                    data: fd,
                    success: function success() {
                        location.href = "/music";
                    }
                });
            }
        });
    }
});

/***/ }),

/***/ 45:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony default export */ __webpack_exports__["a"] = ({
    run: function run() {
        //            var self = this;
        //            this.togglePlay();
        //            this.loadAudio();
        //            this.loadVisualize();
        //            this.loopAudio();
        //            this.setVolume(self);
        //            this.muteVolume(self);
    },
    togglePlay: function togglePlay() {
        $("#playBtn").on("click", function () {
            var audio = $("#player").get(0);

            if (audio.paused) {
                audio.play();
                $(this).text("pause");
            } else {
                audio.pause();
                $(this).text("play_arrow");
            }
        });
    },
    loadAudio: function loadAudio() {
        var audio = $("#player").get(0);
        var seekbar = $("#seekbar");
        var currentTime = $("#currentTime");
        var duration = $("#duration");

        function formatTime(seconds) {
            var minutes = Math.floor(seconds / 60);
            minutes = minutes >= 10 ? minutes : "0" + minutes;
            seconds = Math.floor(seconds % 60);
            seconds = seconds >= 10 ? seconds : "0" + seconds;
            return minutes + ":" + seconds;
        }

        audio.onloadedmetadata = function () {
            seekbar.attr('max', audio.duration);
            duration.text(formatTime(audio.duration));
        };
        seekbar.on('change', function () {
            audio.currentTime = seekbar.val();
        });

        audio.ontimeupdate = function () {
            seekbar.val(audio.currentTime);
            currentTime.text(formatTime(audio.currentTime));
        };

        audio.volume = 0.5;
    },
    loopAudio: function loopAudio() {
        $("#loopBtn").on("click", function () {
            var audio = $("#player").get(0);
            if (audio.loop === false) {
                audio.loop = true;
                $(this).addClass("active");
            } else {
                audio.loop = false;
                $(this).removeClass("active");
            };
        });
    },
    muteVolume: function muteVolume(self) {
        $("#muteBtn").on("click", function () {
            var audio = $("#player").get(0);
            if (audio.muted === false) {
                audio.muted = true;
            } else {
                audio.muted = false;
            };
            self.setVolumeIcon();
        });
    },
    setVolume: function setVolume(self) {
        $("#volume").on("input", function () {
            var audio = $("#player").get(0);
            var volume = $(this).val();
            audio.volume = volume;
            self.setVolumeIcon();
        });
    },
    setVolumeIcon: function setVolumeIcon() {
        var audio = $("#player").get(0);
        var volume = audio.volume;
        var muteBtn = $("#muteBtn");
        if (audio.muted) {
            muteBtn.text("volume_off");
        } else if (volume > 0.6) {
            muteBtn.text("volume_up");
        } else if (volume > 0.1) {
            muteBtn.text("volume_down");
        } else {
            muteBtn.text("volume_mute");
        }
    },
    loadVisualize: function loadVisualize() {
        var audio = $("#player").get(0);
        var context = new AudioContext();
        var src = context.createMediaElementSource(audio);
        var analyser = context.createAnalyser();

        var canvas = document.getElementById("canvas");
        canvas.width = window.innerWidth;
        canvas.height = 300;
        var ctx = canvas.getContext("2d");

        src.connect(analyser);
        analyser.connect(context.destination);

        analyser.fftSize = 256;

        var bufferLength = analyser.frequencyBinCount;

        var dataArray = new Uint8Array(bufferLength);

        var WIDTH = canvas.width;
        var HEIGHT = canvas.height;

        var barWidth = WIDTH / bufferLength * 1.5;
        var barHeight;
        var x = 0;

        function renderFrame() {
            requestAnimationFrame(renderFrame);

            x = 0;

            analyser.getByteFrequencyData(dataArray);

            ctx.fillStyle = "#000";
            ctx.fillRect(0, 0, WIDTH, HEIGHT);

            for (var i = 0; i < bufferLength; i++) {
                barHeight = dataArray[i];

                var r = barHeight + 25 * (i / bufferLength);
                var g = 250 * (i / bufferLength);
                var b = 0;

                ctx.fillStyle = "darkcyan";
                ctx.fillRect(x, HEIGHT - barHeight, barWidth, barHeight);
                x += barWidth + 1;
            }
        }
        renderFrame();
    }
});

/***/ })

/******/ });